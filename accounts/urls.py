from django.urls import path
from accounts.views import user_signup, user_login, log_out


urlpatterns = [
    path("login/", user_login, name="login"),
    path("logout/", log_out, name="logout"),
    path("signup/", user_signup, name="signup"),
]
